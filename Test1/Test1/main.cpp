//
//  main.cpp
//  Test1
//
//  Created by Evan Chapman on 2/23/13.
//  Copyright (c) 2013 Evan Chapman. All rights reserved.
//

#include <iostream>

int main(int argc, const char * argv[])
{

    // insert code here...
    //
    std::cout << "Hello, World! Under source control\n";
    std::cout << "One new line of code , test two, final test";
    std::cout << "Following the final clean and manual commit, here we are!";
    std::cout << "Okay this should be up to date now!";
    return 0;
}

